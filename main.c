/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pibenoit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 22:50:23 by pibenoit          #+#    #+#             */
/*   Updated: 2016/02/28 12:43:49 by pibenoit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include "puissance4.h"
#include <time.h>
#include <stdlib.h>
#include <string.h>

int	ft_check_arg(int argc, char **argv, t_board *game)
{
	if (argc == 3
			&& (int)(game->ln = ft_atoi(argv[1])) >= 6
			&& (int)(game->cl = ft_atoi(argv[2])) >= 7)
		return (1);
	else
		ft_putstr("\033[31mLes dimensions sont incorrectes\033[0m");
	return (0);
}

int	ft_choice_first_player(void)
{
	srand(time(NULL));
	return (rand() % 2);	
}

int		ft_check_win(t_board *game, int j, char c)
{
	int	i;
	int resultat;

	i = ft_search_pos(game, j);
	printf("%d\n", i);
	resultat = 0;
	if (ft_check_w_hor(game, i, j, c))
		resultat = 1;
	else if (ft_check_w_ver(game, i, j, c))
		resultat = 1;
	else if (ft_check_w_diar(game, i, j, 0))
		resultat = 1;
	else if (ft_check_w_dial(game, i, j, 0))
		resultat = 1;
	else if (ft_check_nul(game))
		return (3);
	if (!resultat)
		return (0);
	if ((game->map)[i][j] == 'X')
		return (1);
	return (2);
}

int	main(int ac, char **av)
{
	t_board	game;
	int		end;
	int		arg;
	int		j;

	if (!ft_check_arg(ac, av, &game))
		return (0);
	end = 0;
	ft_init_map(&game);
	arg = ft_choice_first_player();
	while (end == 0)
	{
		if (arg)
			j = ft_ia(&game);
		end = ft_check_win(&game, j, 'O');
		if (end == 0)
		{
			ft_print_map(&game);
			j = ft_player(&game);
			end = ft_check_win(&game, j, 'X');
		}
		if (!arg && end == 0)
			j = ft_ia(&game);
	}
	return (ft_print_win(end, &game));
}
