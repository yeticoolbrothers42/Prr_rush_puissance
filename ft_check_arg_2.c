/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdjeffal <sdjeffal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 22:59:57 by sdjeffal          #+#    #+#             */
/*   Updated: 2016/02/27 22:16:19 by pibenoit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

int	ft_check_arg(int argc, char **argv, int *ln, int *cl)
{
	if (argc == 3
	&& (int)(*ln = ft_atoi(argv[1])) >= 6
	&& (int)(*cl = ft_atoi(argv[2])) >= 7)
		return (1);
	else
		ft_putstr("\033[31mLes dimensions sont incorrectes\033[0m");
	return (0);
}
