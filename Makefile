# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sdjeffal <sdjeffal@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/26 23:16:23 by sdjeffal          #+#    #+#              #
#    Updated: 2016/02/28 07:58:51 by pibenoit         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = puissance4

CC = clang

CFLAG = -Wall -Werror -Wextra

CFILES = ft_map.c ft_print.c ft_player.c bot_play.c main.c ft_check_end_game.c

OPATH = obj/

OFILES = $(CFILES:.c=.o)

OBJ = $(addprefix $(OPATH), $(OFILES))

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): $(OBJ)
	@echo "Compiling library "$(NAME)"..."
	@make all -C libft
	@$(CC) $(OBJ) libft/libft.a -o $(NAME)
	@echo "Compilation success !"
	@echo "---"

$(OPATH)%.o: $(CPATH)%.c
	@mkdir -p $(OPATH)
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	@echo "Deleting the directory /obj..."
	@make clean -C libft
	@rm -rf $(OBJ)
	@rmdir $(OPATH) 2> /dev/null || echo "" > /dev/null
	@echo "Deletion complete !"

fclean: clean
	@make fclean -C libft
	@/bin/rm -rf $(NAME)

re: fclean all
