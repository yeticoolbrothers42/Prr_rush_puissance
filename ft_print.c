/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pibenoit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 23:17:39 by pibenoit          #+#    #+#             */
/*   Updated: 2016/02/28 08:33:02 by pibenoit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "puissance4.h"
/*
int		ft_strlen(char *str)
{
	register int	i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}
*/
int		ft_print_win(int end, t_board *game)
{
	ft_print_map(game);
	ft_free_map(game->map);
	if (end == 1)
		write(1, "\nVous avez gagne\n", 17);
	else if (end == 2)
		write(1, "\nVous avez perdu\n", 17);
	else
		write(1, "\nMatch nul\n", 11);
	return (0);
}

void	ft_putline(int cl)
{
	int		i;

	i = 0;
	while (i < cl)
	{
		write(1, "____", 4);
		i++;
	}
	write(1, "_\n", 2);
}

void	ft_print_map(t_board *game)
{
	int	j;
	int	ln;

	ln = game->ln - 1;
	while (ln >= 0)
	{
		j = 0;
		ft_putline(game->cl);
		while ((game->map)[ln][j] != '\0')
		{
			write(1, "| ", 2);
			if ((game->map)[ln][j] == 'X' || (game->map)[ln][j] == 'O')
				write(1, (game->map)[ln] + j, 1);
			else
				write(1, " ", 1);
			write(1, " ", 1);
			j++;
		}
		write(1, "|\n", 2);
		ln--;
	}
	ft_putline(game->cl);
}
