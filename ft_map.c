/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pibenoit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 22:58:31 by pibenoit          #+#    #+#             */
/*   Updated: 2016/02/28 10:24:44 by pibenoit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>
#include <string.h>
#include "puissance4.h"

void	ft_free_map(char **map)
{
	int		i;

	i = 0;
	while (map[i])
	{
		free(map[i]);
		i++;
	}
	free(map);
}

char    *ft_strset(int len, unsigned char c)
{
	char    *str;
	int     i;

	if (!(str = (char*)malloc(sizeof(char) * len)))
		return (NULL);
	str[len - 1] = '\0';
	i = 0;
	while (i < len - 1)
	{
		str[i] = c;
		i++;
	}
	return (str);
}
/*
int     ft_atoi(char *str)
{
	long long   i;
	int         len;

	if (str == NULL)
		return (0);
	i = 0;
	len = 0;
	while (str[len] != '\0' && str[len] > 47 && str[len] < 58)
	{
		i = i * 10;
		i = i + (str[len] - 48);
		len++;
	}
	return (i);
}
*/
void	ft_clean_memory(char **map, int i)
{
	while (i >= 0)
	{
		free(map[i]);
		i--;
	}
	free(map);
}

void	ft_init_map(t_board *game)
{
	int		i;

	i = 0;
	game->map = (char**)malloc(sizeof(char*) * (game->ln + 1));
	(game->map)[game->ln] = NULL;
	while (i < game->ln)
	{
		if(!((game->map)[i] = ft_strset(game->cl + 1, '.')))
		{
			ft_clean_memory(game->map, i - 1);
			return ;
		}
		i++;
	}
}
