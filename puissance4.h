/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   puissance4.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pibenoit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/27 14:03:22 by pibenoit          #+#    #+#             */
/*   Updated: 2016/02/28 10:18:59 by pibenoit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUISSANCE4_H
# define PUISSANCE4_H

# include "libft/libft.h"

struct s_board
{
	char	**map;
	int		ln;
	int		cl;
	int		profondeur_max;
};

typedef struct s_board t_board;

/*
**	main.c
*/

int		ft_check_win(t_board *game, int i, char c);
int		ft_choice_first_player(void);
int		ft_check_arg(int argc, char **argv, t_board *game);

/*
**	ft_check_game.c
*/

char	ft_check_nul(t_board *game);
int		ft_search_pos(t_board *game, int i);
char	ft_check_w_hor(t_board *game, int i, int j, char c);
char	ft_check_w_ver(t_board *game, int i, int j, char c);
char	ft_check_w_dial(t_board *game, int i, int j, int cpt);
char	ft_check_w_diar(t_board *game, int i, int j, int cpt);

/*
**	ft_map.c
*/

void	ft_free_map(char **map);
char	*strset(int len, unsigned char c);
//int		ft_atoi(char *str);
void	ft_clean_memory(char **map, int i);
void	ft_init_map(t_board *game);

/*
**	ft_print.c
*/

//int		ft_strlen(char *str);
int		ft_print_win(int end, t_board *game);
void	ft_putline(int max);
void	ft_print_map(t_board *game);

/*
**	ft_player.c
*/

void	ft_unplace(int nb, t_board *game);
char	ft_place(int nb, char c, t_board *game);
int		ft_player(t_board *game);

/*
**	bot_play.c
*/

int		ft_min(int profondeur, t_board *game);
int		ft_max(int profondeur, t_board *game);
int		ft_ia(t_board *game);
int		ft_gagnant(t_board *game);
int		ft_eval(t_board *game);

#endif
