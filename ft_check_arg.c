/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdjeffal <sdjeffal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 22:59:57 by sdjeffal          #+#    #+#             */
/*   Updated: 2016/02/27 22:04:10 by pibenoit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

int	check_arg(int argc, char **argv)
{
	if (argc == 3 && (ft_atoi(argv[1]) >= 6 && ft_atoi(argv[2]) >= 7))
	{
		return (1);
	}
	return (0);
}
/*
int main(int argc, char **argv)
{
	int ret;

	ret = check_arg(argc, argv);
	ft_putnendl(ret);
	return (0);
}*/
