/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bot_play.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pibenoit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/27 15:51:15 by pibenoit          #+#    #+#             */
/*   Updated: 2016/02/28 11:11:11 by pibenoit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"
#include <unistd.h>

/*
void	ft_ia(t_board *game)
{
	int		tmp;
	int		maxi;
	int		i;

	i = 0;
	max = -1000;
	while (i < cl)
	{
			if (ft_place(i, 'O', game)
			{
				tmp = ft_min(profondeur - 1, game);
				if (tmp > max)
				{
					max = tmp;
					maxi = i;
				}
				ft_unplace(i, game);
			}
			j++;
		}
		i++;
	}
	ft_place(maxi, 'O', game);
}

int		ft_min(int profondeur, t_board *game)
{
	int		min;
	int		i;
	int		tmp;

	if (profondeur == 0 || ft_gagnant(game) != 0)
		return (ft_eval(game));
	i = 1;
	min = 1000;
	while (i <= game->cl)
	{
		if (ft_place(i, 'X', game))
		{
			tmp = ft_max(profondeur - 1, game);
			if (tmp < min)
				min = tmp;
			ft_unplace(i, game);
		}
		i++;
	}
	return (min);
}

int		ft_max(int profondeur, t_board *game)
{
	int		max;
	int		i;
	int		tmp;

	if (profondeur == 0 || ft_gagnant(game) != 0)
		return (ft_eval(game));
	i = 1;
	max = -1000;
	while (i <= game->cl)
	{
		if (ft_place(i, 'O', game))
		{
			tmp = ft_min(profondeur - 1, game);
			if (tmp > max)
				max = tmp;
			ft_unplace(i, game);
		}
		i++;
	}
	return (max);
}

int		ft_gagnant(t_board *game)
{
	static int	gagnant = 0;
	int			tmp;

	if (gagnant != 0)
	{
		tmp = gagnant;
		gagnant = 0;
		return (tmp);
	}
	if ((gagnant = series(game->map, 'O', 4)) == 0)
		gagnant = series(game->map, 'X', 4));
	return (gagnant);
}

int		ft_eval(t_board *game)
{
	int vainqueur;
	int nb_pions;
	int i;
	int j;

	i = 0;
	while (i < game->ln)
	{
		j = 0;
		while (j < game->cl)
		{
			if (game->map[i][j] != '.')
				nb_pions++;
			j++;
		}
		i++;
	}
	if ((vainqueur = ft_gagnant(game)) != 0)
	{
		if (vainqueur == 1)
			return (1000 - nb_pions);
		if (vainqueur == 2)
			return (nb_pions - 1000);
		return (0);
	}
	return (series(game->map, 'O', 3) - series(game->map, 'X', 3));
}

int		check(
*/

int		ft_ia(t_board *game)
{
	int	i;

	i = 0;
//	write(1, "ok\n", 3);
	while (i < game->cl && !(ft_place(i + 1, 'O', game)))
		i++;
//	write(1, "ok\n", 3);
	return (i);
}
