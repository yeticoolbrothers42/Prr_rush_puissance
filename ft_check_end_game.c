/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_end_game.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sdjeffal <sdjeffal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/27 19:35:47 by sdjeffal          #+#    #+#             */
/*   Updated: 2016/02/28 11:07:49 by pibenoit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

/**void	ft_check_diag_up(t_board game, int *series_j1, int *series_j2, int nb)
{
	int i;
	int cmpt1;
	int cmpt2;

	cmpt1 = 0;
	cmpt2 = 0;
	i = -1;
	while (++i < game.ln)
	{
		if (game.map[i][i] == 'X')
		{
			cmpt1++;
			cmpt2 = 0;
			if (cmpt1 == nb)
				++*series_j1;
		}
		else if (game.map[i][i] == 'O')
		{
			cmpt2++;
			cmpt1 = 0;
			if (cmpt2 == nb)
				++*series_j2;
		}
	}
}

void	ft_check_diag_down(t_board game, int *series_j1, int *series_j2, int nb)
{
	int i;
	int cmpt1;
	int cmpt2;

	cmpt1 = 0;
	cmpt2 = 0;
	i = -1;
	while (++i < game.ln)
	{
		if (game.map[i][2 - i] == 'X')
		{
			cmpt1++;
			cmpt2 = 0;
			if (cmpt1 == nb)
				++*series_j1;
		}
		else if (game.map[i][2 - i] == 'O')
		{
			cmpt2++;
			cmpt1 = 0;
			if (cmpt2 == nb)
				++*series_j2;
		}
	}
}**/

char	ft_check_w_hor(t_board *game, int i, int j, char c)
{
	int	cpt;
	int	col;

	cpt = 0;
	if (j > 2)
		col = j - 3;
	else
		col = 0;
	while (col < game->cl && col < j + 4 && cpt < 4)
	{
		if (game->map[i][col] == c)
			cpt++;
		else
			cpt = 0;
		col++;
	}
	if (cpt == 4)
		return (1);
	return (0);
}

char	ft_check_w_ver(t_board *game, int i, int j, char c)
{
	int	cpt;
	int	lin;

	cpt = 0;
	if (i > 2)
		lin = i - 3;
	else
		lin = 0;
	while (lin < game->ln && lin < i + 4 && cpt < 4)
	{
		if (game->map[lin][j] == c)
			cpt++;
		else
			cpt = 0;
		lin++;
	//	if (cpt == 4)
	//		break ;
	}
	if (cpt == 4)
		return (1);
	return (0);
}

char	ft_check_nul(t_board *game)
{
	int	i;
	int	j;

	i = 0;
	while (i < game->ln)
	{
		j = 0;
		while (j < game->cl)
		{
			if (game->map[i][j] == '.')
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}


char	ft_check_w_dial(t_board *game, int i, int j, int cpt)
{
	int	c;
	int l;
	int min;

	min = (j < i) ? j : i;
	if (min > 2)
	{
		c = j - 3;
		l = i - 3;
	}
	else
	{
		c = j - min;
		l = i - min;
	}
	while (c < game->cl && c < j + 4 && cpt < 4 && l < game->ln && l < i + 4)
	{
		if ((game->map)[l][c] == (game->map)[i][j] && (game->map)[i][j] != '.')
			cpt++;
		else
			cpt = 0;
		c++;
		l++;
	}
	return ((cpt == 4) ? 1 : 0);
}

char	ft_check_w_diar(t_board *game, int i, int j, int cpt)
{
	int	c;
	int l;
	int min;

	min = (j < game->ln - i - 1)  ? j : game->ln - i - 1;
	if (min > 2 && (c = j - 3))
		l = i + 3;
	else
	{
		c = j - min;
		l = i + min;
	}
	while (c < game->cl && c < j + 4 && cpt < 4 && l >= 0 && l > i - 4)
	{
		if ((game->map)[l][c] == (game->map)[i][j] && (game->map)[i][j] != '.')
			cpt++;
		else
			cpt = 0;
		c++;
		l--;
	}
	return ((cpt == 4) ? 1 : 0);
}

int		ft_search_pos(t_board *game, int j)
{
	int		i;

	i = game->ln - 1;
	while (i >= 0 && (game->map)[i][j] == '.')
		i--;
	if (i >= 0)
		return (i);
	return (0);
}
/*
void	ft_check_hori(t_board *game, char c, int nb)
{
	int i;
	int j;
	int cmpt1;
	int cmpt2;

	cmpt1 = 0;
	cmpt2 = 0;
	j = 0;
	i = -1;
	while (++i < game.ln)
	{
		cmpt1 = 0;
		cmpt2 = 0;
		while (j < game.ln)
		{
			if (game.map[i][j++] == 'X')
			{
				cmpt1++;
				cmpt2 = 0;
				if (cmpt1 == nb)
					++*series_j1;
			}
			else if (game.map[i][j++] == 'O')
			{
				cmpt2++;
				cmpt1 = 0;
				if (cmpt2 == nb)
					++*series_j2;
			}
		}
	}
}

void	ft_check_vert(t_board game, int *series_j1, int *series_j2, int nb)
{
	int i;
	int j;
	int cmpt1;
	int cmpt2;

	cmpt1 = 0;
	cmpt2 = 0;
	j = 0;
	i = -1;
	while (++i < game.ln)
	{
		cmpt1 = 0;
		cmpt2 = 0;
		while (j < game.ln)
		{
			if (game.map[j++][i] == 'X')
			{
				cmpt1++;
				cmpt2 = 0;
				if (cmpt1 == nb)
					++*series_j1;
			}
			else if (game.map[j++][i] == 'O')
			{
				cmpt2++;
				cmpt1 = 0;
				if (cmpt2 == nb)
					++*series_j2;
			}
		}
	}
}*/
