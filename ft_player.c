/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_player.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pibenoit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/27 14:28:18 by pibenoit          #+#    #+#             */
/*   Updated: 2016/02/28 10:33:03 by pibenoit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"
#include <sys/uio.h>
#include <string.h>
#include <unistd.h>

void	ft_unplace(int nb, t_board *game)
{
	int	i;

	i = 0;
	while (i < game->ln && (game->map)[i][nb - 1] != '.')
		i++;
	(game->map)[i - 1][nb - 1] = '.';
}

char	ft_place(int nb, char c, t_board *game)
{
	int	i;

	i = 0;
	while (i < game->ln && (game->map)[i][nb - 1] != '.')
		i++;
	if (i != game->ln)
		(game->map)[i][nb - 1] = c;
	else
		return (0);
	return (1);
}

int		ft_player(t_board *game)
{
	char	buf[9];
	int		cpt;
	int		nb;
	int		end;

	end = 1;
	write(1, "A vous de jouer!\n", 17);
	while (end)
	{
		if ((cpt = read(0, buf, 8)) == -1)
			return (-1);
		buf[cpt] = '\0';
		nb = ft_atoi(buf);
		if (nb <= game->cl && nb > 0 && ft_place(nb, 'X', game))
			end = 0;
		else
			write(1, "Votre colonne n'est pas valide\nReessayez\n", 41);
	}
	return (nb - 1);
}			
